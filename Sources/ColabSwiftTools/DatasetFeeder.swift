import Foundation
import SwiftAtomics

public typealias DatasetFeedingFunction<Element> = (_: DatasetSink<Element>) -> Void

/// Feed a dataset using a worker thread.
///
/// This class is not thread safe.
public final class DatasetFeeder<Element> {
    private var feederThread: Thread!
    private var done = false
    
    fileprivate let queue: ConcurrentQueue<Item<Element>>
    fileprivate var shouldStop = AtomicBool(false)
    
    private final class FeederThread: Thread {
        let sink: DatasetSink<Element>
        let feedingFunc: DatasetFeedingFunction<Element>
        
        init(parent: DatasetFeeder<Element>, feedingFunc: @escaping DatasetFeedingFunction<Element>) {
            sink = DatasetSink(parent: parent)
            self.feedingFunc = feedingFunc
        }
        
        override func main() {
            feedingFunc(sink)
            sink.markDone()
        }
    }
    
    private init(capacity: Int) {
        queue = ConcurrentQueue(withCapacity: capacity)
    }
    
    public static func start(capacity: Int, fedBy feedingFunc: @escaping DatasetFeedingFunction<Element>) -> Self {
        let this = Self(capacity: capacity)
        this.feederThread = FeederThread(parent: this, feedingFunc: feedingFunc)
        this.feederThread.start()
        return this
    }
    
    public func stop() {
        shouldStop.store(true, order: .relaxed)
        
        // Unblock `enqueueBlocking` to let the feeding thread quit gracefully
        if queue.count > 0 {
            _ = queue.dequeueBlocking()
        }
        
        done = true
    }
    
    deinit {
        stop()
    }
    
    /// Receive an object. Returns `nil` if the feeding function completed and there are no more objects to return.
    public func nextBlocking() -> Element? {
        if done {
            return nil
        }
        
        switch queue.dequeueBlocking() {
        case .none:
            done = true
            return nil
        case .some(let el):
            return el
        }
    }
}

enum Item<Element> {
    case none
    case some(Element)
}

/// An object passed to and used by a feeder function to send objects to the owner of `DatasetFeeder`.
public final class DatasetSink<Element> {
    weak var parent: DatasetFeeder<Element>?
    
    init(parent: DatasetFeeder<Element>) {
        self.parent = parent
    }
    
    /// Feed an object. Returns `false` if the feeding must be stopped.
    public func feed(_ element: Element) -> Bool {
        return feedInternal(Item.some(element))
    }
    
    /// Notify that there are no more objects to feed.
    fileprivate func markDone() {
        _ = feedInternal(Item.none)
    }
    
    private func feedInternal(_ item: Item<Element>) -> Bool {
        guard let parent = self.parent else { return false }
        
        if parent.shouldStop.load(order: .relaxed) {
            return false
        }
        
        parent.queue.enqueueBlocking(item)
        
        return true
    }
}
