import Foundation
#if canImport(FoundationNetworking)
import FoundationNetworking
#endif

public enum NetTools {}

/// Specifies the overwriting behavior of `NetTools.download`.
public enum OverwriteMode {
    case Overwrite
    case Ignore
    case Fail
}

public enum DownloadError: Error {
    /// The destination file already exists. This can happen only when `OverwriteMode.Fail` is specified.
    case destinationAlreadyExists
}

extension NetTools {
    public static func download(
        from sourceString: String,
        to destinationString: String,
        withOverwriteMode overwriteMode: OverwriteMode = .Overwrite
    ) throws {
        let source = URL(string: sourceString)!
        let destination = URL(fileURLWithPath: destinationString)

        if FileManager().fileExists(atPath: destinationString) {
            switch overwriteMode {
            case .Overwrite:
                break
            case .Ignore:
                return
            case .Fail:
                throw DownloadError.destinationAlreadyExists
            }
        }

        let data = try Data.init(contentsOf: source)
        try data.write(to: destination)
    }
}
