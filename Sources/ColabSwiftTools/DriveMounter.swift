// This module works only if Swift for TensorFlow is in use
#if canImport(TensorFlow)
import Python

public enum DriveMounter {}

extension DriveMounter {
    /// Mount your Google Drive at the specified mountpoint path.
    public static func mount(atPath mountpoint: String) {
        let drive = Python.import("cstsupport")
        drive.mount(mountpoint)
    }
}
#endif
