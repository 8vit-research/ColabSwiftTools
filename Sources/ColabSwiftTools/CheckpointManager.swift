#if canImport(TensorFlow) && canImport(Python)
import Foundation

public enum CheckpointError: Error {
    /// Tried to read a checkpoint file discovered earlier, but it wasn't there by the time it was read.
    case notFound
}

/// Defines a naming convention for checkpoint files. Provides methods for reading and writing
/// checkpoint files.
public final class CheckpointManager {
    let pathPrefix: String
    let lastComponentPrefix: String
    
    public init(pathPrefix: String) throws {
        self.pathPrefix = pathPrefix
        self.lastComponentPrefix = URL(fileURLWithPath: pathPrefix).lastPathComponent + "-"
        
        let parentDir = URL(fileURLWithPath: pathPrefix).deletingLastPathComponent().path
        try FileManager.default.createDirectory(
            atPath: parentDir,
            withIntermediateDirectories: true)
    }
    
    public func checkpointPath(forStep step: Int) -> String {
        return pathPrefix + String(format: "-%07d.npy", step)
    }
    
    func step(fromPath path: String) -> Int? {
        step(fromURL: URL(fileURLWithPath: path))
    }
    
    func step(fromURL url: URL) -> Int? {
        let lastComponent = url.lastPathComponent
        
        if !lastComponent.hasPrefix(lastComponentPrefix) || !lastComponent.hasSuffix(".npy") {
            return nil
        }
        
        let stepStr = lastComponent
            .dropFirst(lastComponentPrefix.count)
            .dropLast(".npy".count)
        
        return Int(stepStr, radix: 10)
    }
    
    /// Read the latest checkpoint file using `CheckpointReader.read`.
    ///
    /// Returns a step number on success. Returns `nil` if the checkpoint file does not exist.
    ///
    /// May throw `CheckpointError.notFound` under a very unlikely circumstance.
    public func readLatest(using function: (CheckpointReader) throws -> Void) throws -> Int? {
        let parentDirUrl = URL(fileURLWithPath: pathPrefix).deletingLastPathComponent()
        let parentDir = parentDirUrl.path
        
        guard let (name, step) = (try FileManager.default.contentsOfDirectory(atPath: parentDir)
            .lazy
            .map { ($0, self.step(fromPath: $0)) }
            .filter { $0.1 != nil }
            .max { $0.1! < $1.1! }
        ) else {
            return nil
        }
        
        let path = parentDirUrl.appendingPathComponent(name).path
        
        guard try CheckpointReader.read(atPath: path, using: function) else {
            throw CheckpointError.notFound
        }
        
        return step!
    }
    
    /// Write a checkpoint file using `CheckpointWriter.write`.
    public func write(step: Int, using function: (CheckpointWriter) throws -> Void) throws {
        let path = checkpointPath(forStep: step)
        try CheckpointWriter.write(atPath: path, using: function)
    }
}

#endif
