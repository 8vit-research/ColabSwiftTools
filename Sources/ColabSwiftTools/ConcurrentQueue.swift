import Foundation

public class ConcurrentQueue<Element> {
    private let cond = NSCondition()
    private var storage: [Element?]
    private var readIndex = 0
    private var _count = 0

    public init(withCapacity capacity: Int) {
        storage = Array(repeating: nil, count: capacity)
    }

    public var count: Int {
        cond.lock()
        defer { cond.unlock() }
        return _count
    }

    public func enqueueBlocking(_ element: Element) {
        cond.lock()
        defer { cond.unlock() }

        while _count >= storage.count {
            cond.wait()
        }

        var writeIndex = readIndex + _count
        if writeIndex >= storage.count {
            writeIndex -= storage.count
        }
        storage[writeIndex] = element
        _count += 1

        cond.broadcast()
    }

    public func dequeueBlocking() -> Element {
        cond.lock()
        defer { cond.unlock() }

        while _count == 0 {
            cond.wait()
        }

        var el: Element? = nil
        swap(&el, &storage[readIndex])

        readIndex += 1
        if readIndex == storage.count {
            readIndex = 0
        }
        _count -= 1

        cond.broadcast()

        return el!
    }
}
