// Multi-threaded use of `Python` (the Python interop) is currently not supported
// (https://github.com/tensorflow/swift/issues/224).
//
// Since `PythonLibrary.loadSymbol` (defined in
// <https://github.com/apple/swift/blob/tensorflow/stdlib/public/Python/PythonLibrary.swift>)
// is `internal`, we are going to get another handle to the Python shared
// library. This solution is horrible but works.
#if canImport(Python) && canImport(TensorFlow)

#if canImport(Darwin)
import Darwin
#elseif canImport(Glibc)
import Glibc
#elseif os(Windows)
// TODO
#endif

import Foundation
import Python

let libraryPathVersionCharacter: Character = ":"
#if canImport(Darwin)
let libraryName = "/usr/local/Frameworks/Python.framework/Versions/:/Python"
#else
let libraryName = "libpython:m.so"
#endif

let pythonLibraryHandle = Array(0...30)
    .reversed()
    .reduce(nil, { (accum, minorVersion) -> UnsafeMutableRawPointer? in
        if let found = accum {
            return found
        } else {
            let path = libraryName
                .split(separator: libraryPathVersionCharacter)
                .joined(separator:"3.\(minorVersion)")
            
            return dlopen(path, RTLD_LAZY | RTLD_GLOBAL)
        }
    })!

func loadPythonSymbol<T>(name: String, type: T.Type = T.self) -> T {
    return unsafeBitCast(dlsym(pythonLibraryHandle, name), to: type)
}

public typealias PyCCharPointer = UnsafePointer<Int8>
public typealias PyThreadStatePointer = UnsafeRawPointer

public let PyEval_InitThreads: @convention(c) () -> Void =
    loadPythonSymbol(name: "PyEval_InitThreads")
public let PyRun_SimpleString: @convention(c) (PyCCharPointer) -> Void =
    loadPythonSymbol(name: "PyRun_SimpleString")
public let PyGILState_Ensure: @convention(c) () -> Int32 =
    loadPythonSymbol(name: "PyGILState_Ensure")
public let PyGILState_Release: @convention(c) (Int32) -> Void =
    loadPythonSymbol(name: "PyGILState_Release")
public let PyEval_SaveThread: @convention(c) () -> PyThreadStatePointer =
    loadPythonSymbol(name: "PyEval_SaveThread")
public let PyEval_RestoreThread: @convention(c) (PyThreadStatePointer) -> Void =
    loadPythonSymbol(name: "PyEval_RestoreThread")

public enum PythonThreading {}

extension PythonThreading {
    /// Enable multi-threading for the Python interpreter.
    ///
    /// Calls `PyEval_SaveThread`. Thus, from this point on, you must acquire the GIL whenever
    /// you interact with the interpreter.
    public static func initialize() {
        print("Python version (TF binding): ", Python.version)

        PyEval_InitThreads()
        PyRun_SimpleString("import sys; print('Python version (our binding): ', sys.version)")

        // Release the main thread
        _ = PyEval_SaveThread()
    }
}

/// The lock guard for Python GIL (Global Interpreter Lock).
public struct PythonGIL {
    let state = PyGILState_Ensure()

    public func unlock() {
        PyGILState_Release(state)
    }

    /// Acquire the GIL and call the given closure.
    public static func ensureIn<ReturnType>(_ fn: () throws -> ReturnType) rethrows -> ReturnType {
        let gil = Self()
        defer { gil.unlock() }
        return try fn()
    }
}
#endif
