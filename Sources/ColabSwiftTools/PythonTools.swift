#if canImport(Python) && canImport(TensorFlow)

import Python

/// Evaluate the specified Python code fragment and return the local variables defined in it
/// as a Python `dict` object.
///
/// This function is useful for parsing the Python code fragment manipulated through
/// Google Colab's form fields.
public func evaluatePythonCodeAndExtractLocals(_ code: String) -> PythonObject {
    PythonGIL.ensureIn { () -> PythonObject in
        let cstsupport = Python.import("cstsupport")
        return cstsupport.eval_and_get_locals(code)
    }
}

#endif
