#if canImport(TensorFlow) && canImport(Python)
import Foundation
import TensorFlow
import Python

// We have to implement the checkpoint management manually until S2TF has an official API.
// Relevant issue: <https://github.com/tensorflow/swift-apis/issues/25>

public enum CheckpointReaderError: Error {
    /// The data stored at the current position is not compatible with the expected data type,
    /// usually indicating the data was serialized from a different data type.
    case schemeMismatch
    /// The file contains malformed data.
    case badFormat
}

/// Restores the state of objects implementing [`KeyPathIterable`] from a file.
///
/// [`KeyPathIterable`]: https://www.tensorflow.org/swift/api_docs/Protocols/KeyPathIterable
public final class CheckpointReader {
    var pyobj: PythonObject!
    var index = 0
    
    /// Open a file for reading.
    ///
    /// Warning: This method internally uses the `pickle` Python module, which is not secure against
    /// erroneous or maliciously constructed data.
    public init?(forReadingAtPath path: String) throws {
        pyobj = PythonGIL.ensureIn { () -> PythonObject in
            let cstsupport = Python.import("cstsupport")
            return cstsupport.np_try_load(path)
        }
        
        if PythonGIL.ensureIn({ pyobj == Python.None }) {
            pyobj = nil
            return nil
        }

        let isValidType = PythonGIL.ensureIn { () -> Bool in
            let numpy = Python.import("numpy")
            return Bool(Python.isinstance(pyobj, numpy.ndarray))!
        }
        
        if !isValidType {
            pyobj = nil
            throw CheckpointReaderError.badFormat
        }
    }
    
    deinit {
        PythonGIL.ensureIn {
            pyobj = nil
        }
    }
    
    /// Call the given closure with an instance of `CheckpointReader`.
    ///
    /// Returns `false` if the file does not exist.
    ///
    /// This is a convenience method.
    public static func read(atPath path: String, using function: (CheckpointReader) throws -> Void) throws -> Bool {
        if let reader = try CheckpointReader(forReadingAtPath: path) {
            try function(reader)
            return true
        } else {
            return false
        }
    }
    
    /// Read state data and store it to the given object.
    ///
    /// Objects must be read in the exact same order as they were written. Otherwise,
    /// `CheckpointReaderError.schemeMismatch` will be thrown.
    public func read<T: KeyPathIterable>(to obj: inout T) throws {
        try PythonGIL.ensureIn {
            let numpy = Python.import("numpy")
            
            for kp in obj.recursivelyAllWritableKeyPaths(to: Tensor<Float>.self) {
                obj[keyPath: kp] = Tensor<Float>(
                    numpy: try readNextPythonObject(expectingType: numpy.ndarray))!
            }
            
            for kp in obj.recursivelyAllWritableKeyPaths(to: Int.self) {
                obj[keyPath: kp] = Int(try readNextPythonObject())!
            }
            
            for kp in obj.recursivelyAllWritableKeyPaths(to: Float.self) {
                obj[keyPath: kp] = Float(try readNextPythonObject())!
            }
            
            for kp in obj.recursivelyAllWritableKeyPaths(to: Double.self) {
                obj[keyPath: kp] = Double(try readNextPythonObject())!
            }
            
            // TODO: other types
            // TODO: throw `schemeMismatch` on conversion failure
        }
    }
    
    public func read(to obj: inout Tensor<Float>) throws {
        try PythonGIL.ensureIn {
            obj = Tensor<Float>(numpy: try readNextPythonObject())!
        }
    }
    
    public func read(to obj: inout Int) throws {
        try PythonGIL.ensureIn {
            obj = Int(try readNextPythonObject())!
        }
    }
    
    public func read(to obj: inout Float) throws {
        try PythonGIL.ensureIn {
            obj = Float(try readNextPythonObject())!
        }
    }
    
    public func read(to obj: inout Double) throws {
        try PythonGIL.ensureIn {
            obj = Double(try readNextPythonObject())!
        }
    }
    
    func readNextPythonObject() throws -> PythonObject {
        if index >= Int(Python.len(pyobj))! {
            throw CheckpointReaderError.schemeMismatch
        }
        let ret = pyobj[index]
        index += 1
        return ret
    }
    
    func readNextPythonObject(expectingType: PythonObject) throws -> PythonObject {
        let obj = try readNextPythonObject()
        if !Bool(Python.isinstance(obj, expectingType))! {
            throw CheckpointReaderError.schemeMismatch
        }
        return obj
    }
}

/// Persists the state of objects implementing [`KeyPathIterable`] to a file.
///
/// [`KeyPathIterable`]: https://www.tensorflow.org/swift/api_docs/Protocols/KeyPathIterable
///
/// Make sure to call `close` after using it.
public final class CheckpointWriter {
    let path: String
    var open = true
    var pyobj: [PythonObject] = []
    
    /// Open a file for writing.
    ///
    /// A `.npy` extension will be automatically appended if `path` doesn't have one already.
    public init(forWritingAtPath path: String) {
        self.path = path
    }
    
    /// Call the given closure with an instance of `CheckpointWriter`, automatically
    /// closing it after being done writing objects.
    ///
    /// This is a convenience method.
    public static func write(atPath path: String, using function: (CheckpointWriter) throws -> Void) throws {
        let writer = CheckpointWriter(forWritingAtPath: path)
        try function(writer)
        try writer.close()
    }
    
    deinit {
        PythonGIL.ensureIn {
            pyobj = []
        }
    }
    
    /// Write the state data of the given object.
    public func write<T: KeyPathIterable>(_ obj: T) {
        for kp in obj.recursivelyAllWritableKeyPaths(to: Tensor<Float>.self) {
            write(obj[keyPath: kp])
        }
        
        for kp in obj.recursivelyAllWritableKeyPaths(to: Int.self) {
            write(obj[keyPath: kp])
        }
        
        for kp in obj.recursivelyAllWritableKeyPaths(to: Float.self) {
            write(obj[keyPath: kp])
        }
        
        for kp in obj.recursivelyAllWritableKeyPaths(to: Double.self) {
            write(obj[keyPath: kp])
        }
    }
    
    public func write(_ obj: Tensor<Float>) {
        PythonGIL.ensureIn {
            pyobj.append(obj.makeNumpyArray())
        }
    }
    
    public func write(_ obj: Int) {
        PythonGIL.ensureIn {
            pyobj.append(PythonObject(obj))
        }
    }
    
    public func write(_ obj: Float) {
        PythonGIL.ensureIn {
            pyobj.append(PythonObject(obj))
        }
    }
    
    public func write(_ obj: Double) {
        PythonGIL.ensureIn {
            pyobj.append(PythonObject(obj))
        }
    }
    
    /// Finish writing the file.
    public func close() throws {
        if open {
            open = false
            PythonGIL.ensureIn {
                // TODO: Handle errors
                let numpy = Python.import("numpy")
                numpy.save(path, numpy.array(pyobj))
                pyobj = []
            }
        }
    }
}

#endif
