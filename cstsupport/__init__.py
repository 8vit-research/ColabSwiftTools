import getpass, os, time, numpy
from google.colab import drive

_auth_path = "/content/auth.txt"

def _my_getpass(prompt='Password: ', stream=None):
    print(prompt)
    print("(upload the response to %s)" % _auth_path)

    while not os.path.exists(_auth_path):
        time.sleep(0.1)
    time.sleep(0.5)

    with open(_auth_path) as f:
        resp = f.read().strip()

    print("Got it! The response length is %d." % len(resp))
    os.unlink(_auth_path)
    return resp

def mount(at_path):
    # Override the default `getpass.getpass`, which doesn't work in Colab + Swift
    old_getpass = getpass.getpass
    getpass.getpass = _my_getpass

    try:
        drive.mount(at_path)
    finally:
        getpass.getpass = old_getpass

def np_try_load(at_path):
    try:
        return numpy.load(at_path, allow_pickle=True)
    except IOError:
        return None

def eval_and_get_locals(stmt):
    exec(stmt)
    obj = locals()
    del obj['stmt']
    return obj
