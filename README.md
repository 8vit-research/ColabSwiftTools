# ColabSwiftTools

Tools for [Swift for TensorFlow](https://github.com/tensorflow/swift) running in [Google Colaboratory](https://research.google.com/colaboratory/)

    %system git clone https://gitlab.com/8vit-research/ColabSwiftTools.git --depth 1
    %system cd ColabSwiftTools; git fetch; git reset --hard origin/master
    %install '.package(path: "/content/ColabSwiftTools")' ColabSwiftTools

    import Foundation
    Python.import("sys").path.append(Process().currentDirectoryURL!.path + "/ColabSwiftTools")

    PythonThreading.initialize()
    %include "ColabSwiftTools/EnableIPythonDisplay.swift"
