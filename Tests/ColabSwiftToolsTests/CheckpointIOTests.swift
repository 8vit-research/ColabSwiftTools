import Dispatch
import XCTest
@testable import ColabSwiftTools

#if canImport(TensorFlow) && canImport(Python)
import TensorFlow
import Python

struct TestModel: KeyPathIterable, Equatable {
    var weight1 = Tensor<Float>(zeros: [3])
    var weight2 = Tensor<Float>(zeros: [3])
    var ival = 0
    var fval = 0.0
}
#endif

final class CheckpointIOTests: XCTestCase {
    func testRoundtrip() {
        #if canImport(TensorFlow) && canImport(Python)
        initializePython()
        
        var model = TestModel()
        model.weight1 = Tensor<Float>([1, 2, 3])
        model.weight2 = Tensor<Float>([4, 5])
        model.ival = 42
        model.fval = 3.14
        
        let path = "/tmp/ColabSwiftToolsCheckpointIOTestTmp.npy"
        defer {
            do {
                try FileManager.default.removeItem(atPath: path)
            } catch {
                // ignore the error
            }
        }
        
        try! CheckpointWriter.write(atPath: path) { writer in
            writer.write(model)
            writer.write(model)
        }
        
        var model2 = TestModel()
        var model3 = TestModel()
        guard (try! CheckpointReader.read(atPath: path) { reader in
            try reader.read(to: &model2)
            try reader.read(to: &model3)
        }) else {
            XCTFail("`read` returned `false`")
            return
        }
        
        XCTAssertEqual(model2, model)
        XCTAssertEqual(model3, model)
        #endif
    }
    
    static var allTests = [
        ("testRoundtrip", testRoundtrip),
    ]
}
