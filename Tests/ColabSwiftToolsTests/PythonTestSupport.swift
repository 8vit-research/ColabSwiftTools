#if canImport(TensorFlow) && canImport(Python)
import Python
import Foundation
import ColabSwiftTools

func initializePython() {
    PythonThreading.initialize()
    
    PythonGIL.ensureIn {
        let toolsRoot = URL(fileURLWithPath: #file)
            .deletingLastPathComponent()
            .deletingLastPathComponent()
            .deletingLastPathComponent()
        Python.import("sys").path.append(toolsRoot.path)
    }
}
#endif
