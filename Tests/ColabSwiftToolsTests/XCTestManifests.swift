import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(CheckpointIOTests.allTests),
        testCase(ConcurrentQueueTests.allTests),
        testCase(DatasetFeederTests.allTests),
        testCase(PythonToolsTests.allTests),
    ]
}
#endif
