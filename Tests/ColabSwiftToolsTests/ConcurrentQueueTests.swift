import Dispatch
import XCTest
@testable import ColabSwiftTools

final class ConcurrentQueueTests: XCTestCase {
    func testNonblockingOperation() {
        let queue = ConcurrentQueue<Int>(withCapacity: 4)
        queue.enqueueBlocking(1)
        queue.enqueueBlocking(2)
        queue.enqueueBlocking(3)
        queue.enqueueBlocking(4)
        XCTAssertEqual(queue.dequeueBlocking(), 1)
        XCTAssertEqual(queue.dequeueBlocking(), 2)
        XCTAssertEqual(queue.dequeueBlocking(), 3)
        XCTAssertEqual(queue.dequeueBlocking(), 4)
    }

    func testBlockingDequeue() {
        let queue = ConcurrentQueue<Int>(withCapacity: 4)

        DispatchQueue.global().async {
            XCTAssertEqual(queue.dequeueBlocking(), 1)
            XCTAssertEqual(queue.dequeueBlocking(), 2)
            XCTAssertEqual(queue.dequeueBlocking(), 3)
            XCTAssertEqual(queue.dequeueBlocking(), 4)
        }

        Thread.sleep(forTimeInterval: 0.1)
        queue.enqueueBlocking(1)
        Thread.sleep(forTimeInterval: 0.1)
        queue.enqueueBlocking(2)
        Thread.sleep(forTimeInterval: 0.1)
        queue.enqueueBlocking(3)
        Thread.sleep(forTimeInterval: 0.1)
        queue.enqueueBlocking(4)
    }

    func testBlockingEnqueue() {
        let queue = ConcurrentQueue<Int>(withCapacity: 4)

        queue.enqueueBlocking(1)
        queue.enqueueBlocking(2)
        queue.enqueueBlocking(3)
        queue.enqueueBlocking(4)

        DispatchQueue.global().async {
            queue.enqueueBlocking(5)
            queue.enqueueBlocking(6)
        }

        Thread.sleep(forTimeInterval: 0.1)
        XCTAssertEqual(queue.dequeueBlocking(), 1)
        XCTAssertEqual(queue.dequeueBlocking(), 2)
        XCTAssertEqual(queue.dequeueBlocking(), 3)
        XCTAssertEqual(queue.dequeueBlocking(), 4)

        XCTAssertEqual(queue.dequeueBlocking(), 5)
        XCTAssertEqual(queue.dequeueBlocking(), 6)
    }

    static var allTests = [
        ("testNonblockingOperation", testNonblockingOperation),
        ("testBlockingDequeue", testBlockingDequeue),
        ("testBlockingEnqueue", testBlockingEnqueue),
    ]
}
