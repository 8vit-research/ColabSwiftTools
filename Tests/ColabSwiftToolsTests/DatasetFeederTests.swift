import Dispatch
import XCTest
@testable import ColabSwiftTools

final class DatasetFeederTests: XCTestCase {
    func testSimple() {
        let feeder = DatasetFeeder<Int>.start(capacity: 2) { sink in
            for i in 1...4 {
                guard sink.feed(i) else { return }
                Thread.sleep(forTimeInterval: 0.1)
            }
        }
        
        for i in 1...4 {
            XCTAssertEqual(feeder.nextBlocking(), i)
        }
        XCTAssertEqual(feeder.nextBlocking(), nil)
    }
    
    func testStop() {
        let feeder = DatasetFeeder<Int>.start(capacity: 2) { sink in
            for i in 1...64 {
                guard sink.feed(i) else { return }
                Thread.sleep(forTimeInterval: 0.1)
            }
            
            XCTFail("Feeding did not stop despite the feeder was dropped")
        }
        
        for i in 1...4 {
            XCTAssertEqual(feeder.nextBlocking(), i)
        }
    }

    static var allTests = [
        ("testSimple", testSimple),
        ("testStop", testStop),
    ]
}
