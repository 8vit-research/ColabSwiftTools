import Dispatch
import XCTest
@testable import ColabSwiftTools

#if canImport(TensorFlow) && canImport(Python)
import Python
#endif

final class PythonToolsTests: XCTestCase {
    func testSimple() {
        #if canImport(TensorFlow) && canImport(Python)
        PythonGIL.ensureIn {
            let obj = evaluatePythonCodeAndExtractLocals("""
                var1 = "value"
                var2 = 42
                """)

            XCTAssertEqual(obj["var1"], "value")
            XCTAssertEqual(obj["var2"], 42)
        }
        #endif
    }

    static var allTests = [
        ("testSimple", testSimple),
    ]
}
