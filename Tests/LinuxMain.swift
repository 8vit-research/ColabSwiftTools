import XCTest

import ColabSwiftToolsTests

var tests = [XCTestCaseEntry]()
tests += CheckpointIOTests.allTests()
tests += ConcurrentQueueTests.allTests()
tests += DatasetFeederTests.allTests()
tests += PythonToolsTests.allTests()
XCTMain(tests)
