// swift-tools-version:5.1

import PackageDescription

let package = Package (
    name: "ColabSwiftTools",
    products: [
        .library(
            name: "ColabSwiftTools",
            type: .dynamic,
            targets: ["ColabSwiftTools"]),
    ],
    dependencies: [
        .package(
            url: "https://github.com/glessard/swift-atomics.git",
            .revision("ce883773755a062c22b3dd6ae86b197b76a6933a"))
    ],
    targets: [
        .target(
            name: "ColabSwiftTools",
            dependencies: ["SwiftAtomics"]),
        .testTarget(
            name: "ColabSwiftToolsTests",
            dependencies: ["ColabSwiftTools"]),
    ]
)
